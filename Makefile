LDFLAGS := \
  -X main.Version=$(shell cat VERSION)

# Avoid dynamic linking which doesn't work when building a scratch image
CGO_ENABLED = 0
export CGO_ENABLED

# Respect TARGET* variables defined by docker
# see https://docs.docker.com/engine/reference/builder/#automatic-platform-args-in-the-global-scope
GOOS = $(TARGETOS)
GOARCH = $(TARGETARCH)
export GOOS
export GOARCH

.DEFAULT: all

all: jwt-authorizer

jwt-authorizer: cmd/jwt-authorizer/main.go
	go build -ldflags "$(LDFLAGS)" ./cmd/jwt-authorizer

deb:
	docker buildx build --target deb -f .pipeline/blubber.yaml --output type=local,dest=. .

clean:
	-rm jwt-authorizer
	-rm jwt-authorizer_*
