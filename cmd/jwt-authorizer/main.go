package main

import (
	"bytes"
	"context"
	"fmt"
	"io/fs"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"path"
	"strconv"
	"strings"
	"text/template"
	"time"

	"github.com/lestrrat-go/jwx/jwk"
	"github.com/lestrrat-go/jwx/jwt"
	"github.com/pborman/getopt/v2"
	"github.com/pkg/errors"
)

const (
	serviceName = "jwt_authorizer"

	// See https://www.rfc-editor.org/rfc/rfc6750#section-3.1
	errorInvalidRequest = "invalid_request"
	errorInvalidToken   = "invalid_token"
)

var (
	showHelp      = getopt.BoolLong("help", 'h', "Show help/usage")
	showVersion   = getopt.BoolLong("version", 'v', "Show version")
	keysURL       = getopt.StringLong("keys-url", 'k', "https://gitlab.wikimedia.org/oauth/discovery/keys", "URL to query for JWT keys")
	issuers       = getopt.ListLong("issuers", 'i', "https://gitlab.wikimedia.org", "Comma-separated list of accepted JWT issuers")
	listen        = getopt.StringLong("listen", 'l', "tcp://0.0.0.0:1337", "tcp://address:port or unix:///socket/path to listen on")
	socketMode    = getopt.StringLong("socket-mode", 'm', "", "Permission/mode of created unix socket")
	realm         = getopt.StringLong("realm", 'r', "https://gitlab.wikimedia.org/jwt/auth", "Realm used in 401 challenge")
	requestPrefix = getopt.StringLong("request-prefix", 'p', "/", "Request path prefix to be stripped")
	validation    = getopt.StringLong("validation-template", 't', "", "Go template used to validate tokens")
	templateFuncs = template.FuncMap{
		"hasPrefix": func(s, prefix string) bool { return strings.HasPrefix(s, prefix) },
	}
	acceptedIssuers map[string]bool
	Version         string
)

func main() {
	getopt.Parse()

	if *showHelp {
		getopt.Usage()
		os.Exit(0)
	}

	if *showVersion {
		fmt.Println(Version)
		os.Exit(0)
	}

	listenURL, err := url.Parse(*listen)
	if err != nil {
		log.Printf("failed to parse listen URL %#v", *listen)
		os.Exit(1)
	}

	var socketFileMode fs.FileMode = 0
	if *socketMode != "" {
		socketModeInt64, err := strconv.ParseUint(*socketMode, 8, 32)
		if err != nil {
			log.Printf("bad socket file mode given %s: %v", *socketMode, err)
			os.Exit(1)
		}
		socketFileMode = fs.FileMode(socketModeInt64)
	}

	var validationTmpl *template.Template
	if *validation != "" {
		validationTmpl, err = template.New(path.Base(*validation)).Funcs(templateFuncs).ParseFiles(*validation)
		if err != nil {
			log.Printf("failed to parse template %#v: %v", *validation, err)
			os.Exit(1)
		}
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	log.Printf("getting the GitLab JWT public key set from %s...", *keysURL)

	jwks := jwk.NewAutoRefresh(ctx)
	jwks.Configure(*keysURL, jwk.WithMinRefreshInterval(5*time.Minute))

	_, err = jwks.Refresh(ctx, *keysURL)
	if err != nil {
		log.Fatalf("failed to refresh JWKs from %s: %v\n", *keysURL, err)
	}

	fetchKeys := func() (jwk.Set, error) {
		log.Printf("fetching JWKs...")
		return jwks.Fetch(ctx, *keysURL)
	}

	acceptedIssuers = map[string]bool{}
	for _, issuer := range *issuers {
		acceptedIssuers[issuer] = true
	}

	http.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		if len(*requestPrefix) > len(req.URL.String()) {
			w.Header().Add(
				"WWW-Authenticate",
				fmt.Sprintf(
					"Bearer realm=%q, service=%q, error=%q",
					*realm, serviceName, errorInvalidRequest,
				),
			)
			// See https://www.rfc-editor.org/rfc/rfc6750#section-3.1
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		scope := "repository:" + req.URL.String()[1:] + ":push,pull"

		if err := authorize(req, fetchKeys, validationTmpl); err != nil {
			w.Header().Add(
				"WWW-Authenticate",
				fmt.Sprintf(
					"Bearer realm=%q, service=%q, scope=%q, error=%q, error_description=%q",
					*realm, serviceName, scope, errorInvalidToken, err,
				),
			)
			w.WriteHeader(http.StatusUnauthorized)
		} else {
			log.Println("authorized: jwt is valid and is authorized")
			w.WriteHeader(http.StatusOK)
		}

		return
	})

	listenAddress := listenURL.Host
	if listenURL.Scheme == "unix" {
		listenAddress = listenURL.Path

		// If a unix socket already exists, try to remove it
		if _, err := os.Stat(listenAddress); !errors.Is(err, os.ErrNotExist) {
			log.Printf("warning: deleting existing unix socket %s", listenAddress)
			os.Remove(listenAddress)
		}
	}

	log.Printf("listening on %s", *listen)

	listener, err := net.Listen(listenURL.Scheme, listenAddress)
	if err != nil {
		log.Fatalf("failed to bind to %s: %v", *listen, err)
	}

	cleanup := func() {
		log.Printf("deleting unix socket %s", listenAddress)
		os.Remove(listenAddress)
	}

	if listenURL.Scheme == "unix" && socketFileMode > 0 {
		if err = os.Chmod(listenAddress, socketFileMode); err != nil {
			cleanup()
			log.Fatalf("failed to change permissions of %s: %v", listenAddress, err)
		}
	}

	cleanupChan := make(chan os.Signal, 1)
	signal.Notify(cleanupChan, os.Interrupt)
	go func() {
		for _ = range cleanupChan {
			cleanup()
			os.Exit(2)
		}
	}()

	http.Serve(listener, nil)

	cleanup()
	os.Exit(3)
}

// isAuthorized validates the token within the given Authorization header
// using the previously retrieved key set and compares the project_path claim
// to the request path.
//
// Most of this was ripped off from
// https://github.com/rgl/gitlab-ci-validate-jwt/blob/master/main.go
//
func authorize(req *http.Request, fetchKeys func() (jwk.Set, error), validationTmpl *template.Template) error {
	log.Printf("authorizing request: %s %s", req.Method, req.RequestURI)

	keySet, err := fetchKeys()
	if err != nil {
		return errors.Wrapf(err, "failed to fetch JWKs from %s", *keysURL)
	}

	if keySet.Len() < 1 {
		return errors.Errorf("%s did not return any key", *keysURL)
	}

	log.Printf("validating JWT...")

	_, err = jwt.ParseHeader(
		req.Header,
		"Authorization",
		jwt.WithKeySet(keySet),
		jwt.WithValidate(true),
		jwt.WithRequiredClaim("project_path"),
		// Validate issuer
		jwt.WithValidator(jwt.ValidatorFunc(func(_ context.Context, token jwt.Token) error {
			tokenIssuer := token.Issuer()
			if acceptedIssuers[tokenIssuer] {
				return nil
			}

			return jwt.NewValidationError(
				fmt.Errorf(`unknown issuer ("iss"): %s`, tokenIssuer),
			)
		})),
		// Validate using the template
		jwt.WithValidator(jwt.ValidatorFunc(func(_ context.Context, token jwt.Token) error {
			if validationTmpl == nil {
				return nil
			}

			result := new(bytes.Buffer)
			err = validationTmpl.Execute(
				result,
				struct {
					Token   jwt.Token
					Claims  map[string]interface{}
					Request string
				}{
					Token:   token,
					Claims:  token.PrivateClaims(),
					Request: req.URL.String()[len(*requestPrefix):],
				},
			)

			if err != nil {
				return jwt.NewValidationError(fmt.Errorf(
					`validation template %v failed to execute: %v`,
					*validation, err,
				))
			}

			invalidMessage := strings.TrimSpace(result.String())
			if invalidMessage != "" {
				return jwt.NewValidationError(fmt.Errorf(`token is invalid: %s`, invalidMessage))
			}

			return nil
		})),
	)

	return err
}
