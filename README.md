# JSON Web Token Authenticator

A small stateless HTTP server that validates (GitLab) JSON Web Tokens for
arbitrary service integration.

It provides remote key fetching with local caching and can make any number of
arbitrary assertions on token payloads using a given Go template.

(It always validates the signature and expiry.)

## Step 1: Write a template

If the validation template outputs anything other than whitespace,
it's considered a validation failure and the output is used as the
message.  Blank or all-whitespace output is considered to be
successful validation.

```tmpl
{{ $project_path := index .Claims "project_path" }}
{{ if not $project_path }}
there is no "project_path" claim present
{{ else if not (or (eq .Request $project_path) (hasPrefix .Request ($project_path | printf "%s/"))) }}
requested resource is not equal to or beneath the project path {{ $project_path }}
{{ end }}
```

## Step 2: Run the server

    
```sh
$ ./jwt-authorizer -t gitlab-project-validation.tmpl 
2022/05/20 15:38:36 getting the GitLab JWT public key set from https://gitlab.wikimedia.org/-/jwks...
2022/05/20 15:38:37 listening on tcp://0.0.0.0:1337
```

## Step 3: Authorize against the server (or have your web server do it)

```sh
$ curl -D - -H "Authorization: Bearer $(cat token)" http://localhost:1337/URMEHGAWD/repos/releng/blubber/foo
HTTP/1.1 401 Unauthorized
Www-Authenticate: Bearer realm="https://gitlab.wikimedia.org/jwt/auth", service="jwt_authorizer", scope="repository:foo/repos/releng/blubber/foo:push,pull"
Date: Fri, 20 May 2022 22:38:49 GMT
Content-Length: 0
```

The server says...

```sh
2022/05/20 15:38:49 authorizing request: GET /foo/repos/releng/blubber/foo
2022/05/20 15:38:49 fetching JWKs...
2022/05/20 15:38:49 validating JWT...
2022/05/20 15:38:49 failed to parse and validate the jwt: token is invalid: requested resource is not equal to or beneath the project path repos/releng/blubber
```

Let's try again.

```sh
$ curl -D - -H "Authorization: Bearer $(cat token)" http://localhost:1337/repos/releng/blubber/foo
HTTP/1.1 200 OK
Date: Fri, 20 May 2022 22:44:22 GMT
Content-Length: 0
```

Yay.

## It's fairly general

There are still some assumptions about working with GitLab, but it is nearly
general and has many runtime options.

```
Usage: jwt-authorizer [-h] [-i value] [-k value] [-l value] [-m value] [-p value] [-r value] [-t value] [parameters ...]
 -h, --help         show help/usage
 -i, --issuers=values
                    Comma-separated list of accepted JWT issuers
                    [https://gitlab.wikimedia.org]
 -k, --keys-url=value
                    URL to query for JWT keys
                    [https://gitlab.wikimedia.org/oauth/discovery/keys]
 -l, --listen=value
                    tcp://address:port or unix:///socket/path to listen on
                    [tcp://0.0.0.0:1337]
 -m, --socket-mode=value
                    Permission/mode of created unix socket
 -p, --request-prefix=value
                    Request path prefix to be stripped [/]
 -r, --realm=value  Realm used in 401 challenge
                    [https://gitlab.wikimedia.org/jwt/auth]
 -t, --validation-template=value
                    Go template used to validate tokens
```
